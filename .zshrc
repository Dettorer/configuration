# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  # source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# The following lines were added by compinstall

zstyle ':completion:*' completer _complete _ignored _correct _approximate
zstyle ':completion:*' group-name ''
zstyle ':completion:*' insert-unambiguous true
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}'
zstyle ':completion:*' menu select=1
zstyle ':completion:*' original true
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true

# {{{ options
setopt append_history           # append history list to the history file (important for multiple parallel zsh sessions!)
setopt extended_history         # save each command's beginning timestamp and the duration to the history file
setopt histignorespace          # remove command lines from the history list when
                                # the first character on the line is a space
setopt auto_cd                  # if a command is issued that can't be executed as a normal command,
                                # and the command is the name of a directory, perform the cd command to that directory
setopt longlistjobs             # display PID when suspending processes as well
setopt noextendedglob           # disable the special meaning of #, ^ and ~ (was enabled by grml)
# }}}

autoload -Uz compinit
compinit
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=200000
SAVEHIST=200000
bindkey -e
# End of lines configured by zsh-newuser-install

# Aliases
source ~/.aliases

# Pager
export PAGER=`which most`

# Editor
if type nvim > /dev/null 2>&1; then
    export EDITOR=nvim
else
    export EDITOR=vim
fi

# {{{ PATH
# Path: godi
export PATH=/opt/godi/bin:/opt/godi/sbin:$PATH

# Path: my scripts (https://bitbucket.org/Dettorer/scripts)
export PATH=$PATH:~/configuration/scripts/links

# Path: some programs installed in ~/bin
export PATH=~/.local/bin:$PATH

# Path: Ruby gems
export PATH=$PATH:~/.gem/ruby/1.9.1/bin

# Path: sbin
export PATH=$PATH:/sbin:/usr/sbin

# Path: quartus
export PATH=$PATH:~/altera/14.1/quartus/bin

# Path: DEVKITARM
export DEVKITPRO=/opt/devkitpro
export DEVKITARM=$DEVKITPRO/devkitARM
export PATH=$PATH:$DEVKITARM/bin
export MANPATH=$MANPATH:$DEVKITARM/man

# Path: CUDA
export PATH=$PATH:/opt/cuda/bin

# Path: locally installed rust packages with `cargo install`
export PATH=~/.cargo/bin:$PATH
# }}}

# NNTP server
export NNTPSERVER='snews://news.cri.epita.fr'

# TERM
# export TERM='screen-256color'

# SSH_AGENT
export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"

# ls and zsh what colors they can use by looking at the $TERM variable directly
# or indirectly through dicolors. Alacritty's TERM name isn't properly
# recognized, but dircolors recognizes any TERM that has "color" in its name,
# and we can tell zsh to use the same colors as ls
if [[ $TERM == alacritty ]]; then
    eval $(env TERM=alacritty-color dircolors)
    zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
fi


# vim:filetype=zsh foldmethod=marker

# opam configuration
test -r /home/dettorer/.opam/opam-init/init.zsh && . /home/dettorer/.opam/opam-init/init.zsh > /dev/null 2> /dev/null || true

# Theme and prompt
source /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
if [ -e /home/dettorer/.nix-profile/etc/profile.d/nix.sh ]; then . /home/dettorer/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer

eval "$(direnv hook zsh)"

if type zoxide > /dev/null 2>&1; then
    eval "$(zoxide init zsh)"
fi

export QSYS_ROOTDIR="/home/dettorer/.cache/pikaur/build/quartus-free/pkg/quartus-free-quartus/opt/intelFPGA/21.1/quartus/sopc_builder/bin"
export PYENV_ROOT="$HOME/.pyenv"
[[ -d $PYENV_ROOT/bin ]] && export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"
