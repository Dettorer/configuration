Dettorer's dotfiles
===================

DEPRECATION WARNING
-------------------

I'm gradually abandonning this repository in favor of a NixOS configuration that
includes all my system configuration and home dotfiles.

New repository for my configurations: <https://github.com/Dettorer/NixOS-infra>.
It's currently hard to read and use if you're not on NixOS (or at least using
nix) but I'll try to find a way to automatically commit the generated dotfiles
for non-NixOS users.

About
-----

This is the repository containing my various configuration files

Installation
------------

`./install.zsh` should be enough to fetch submodules and prompt for the creation
of a symlink for each config file.

This repository uses submodules to track the vim plugin manager (vundle) and my
scripts repository.
If you don't know what git submodules are, you can find information
[here](http://git-scm.com/book/en/Git-Tools-Submodules).

You can either clone the repository with --recursive option or issue
`git submodule init` and `git submodule update` right after cloning. The
`install.zsh` script also does this.

Terminfo
--------

Some tools need some extra terminfo data to know that my terminal (alacritty)
supports truecolor. This is emacs' case. For this I create a new terminfo for
alacritty that adds those information, but these need to be compiled by running
the following command:

```console
tic -x -o ~/.terminfo alacritty-24bit.terminfo
```

The tools that need this explicit 24b information can then be launched this way:

```console
TERM=alacritty-24bit emacs
```

Which is one of my aliases

Screenshots
-----------

![empty workspace](screenshots/screen-empty.png)
![terminal alone](screenshots/screen-terminal.png)
![vim, tmux and browser](screenshots/screen-vim-tmux-gaps.png)
