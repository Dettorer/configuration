#! /bin/sh

i3-msg "workspace 1"
    i3-msg "exec firefox"

i3-msg "workspace 2"
    i3-msg "exec urxvt -e zsh -ic 'irc && zsh'"

i3-msg "workspace 3"
    i3-msg "exec urxvt -e zsh -ic 'mail && zsh'"

# Skype is forced to be on workspace 4 anyway but it's clearer
i3-msg "workspace 4"
    i3-msg "exec skype"

i3-msg "workspace 5"
    i3-msg "exec amarok"

# Go back to first workspace and launch chromium
i3-msg "workspace 1"
