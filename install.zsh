#! /usr/bin/env zsh

# This script is a badly upgraded version of Chewie's one
# (https://github.com/Chewie/configs/blob/master/install.zsh)

## Warning: this script assumes that you are using zsh and running it from
## its directory (ie : ./install.zsh)
## This is a quick and dirty script, don't expect anything fancy from it

# Set options related to globbing, for the last line
setopt extendedglob
setopt glob_dots

# Synchronize the git submodules
git submodule init
git submodule update

function install_conf {
    file=$1
    dest=$2

    read -q "REPLY?Install $file as $dest/$file? [y/N]"
    if [ $REPLY != "n" ]; then
        echo
        if [ -e $dest/$file ]; then
            read -q "REPLY?$dest/$file already exists, should back it up in $PWD/backup? [y/N]"
            if [ $REPLY != "n" ]; then
                echo
                mkdir -p $PWD/backup
                mv $dest/$file $PWD/backup/
            else
                echo
                continue
            fi
        fi

        ln -s $PWD/$file $dest/$file
    else
        echo
    fi
}

# Create top-level symlinks
for f in *~*.git~*.gitmodules~*install.zsh~*README.md~*backup~*config~*alacritty-24bit.terminfo~*screenshots; do
    install_conf $f $HOME
done

# Create .config symlinks
cd config
for f in *~*backup; do
    install_conf $f $HOME/.config
done
