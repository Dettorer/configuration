" It's a node project
autocmd BufRead,BufNewFile ~/work/hector/* set makeprg=npm\ start
let g:ctrlp_custom_ignore = '\v[\/](dist|node_modules|.git)$'
