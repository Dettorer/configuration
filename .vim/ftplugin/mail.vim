" Wrap mail in 72 columns
setlocal textwidth=72

" Remove quoted signature
autocmd BufRead /tmp/mutt* :g/^> --\s$/,/^$/-1c
autocmd BufRead *.claws-mail/tmp/tmpmsg.* :g/^> --\s$/,/^$/-1c


" Remove trailing whitespaces, except for signatures
" TODO: change (.*--\s) for ([> ]*-- ) ?
autocmd BufRead /tmp/mutt* :%s/^\(\(.*--\s\)\@!.*\)\s\+$/\1/
autocmd BufRead *.claws-mail/tmp/tmpmsg.* :%s/^\(\(.*--\s\)\@!.*\)\s\+$/\1/
