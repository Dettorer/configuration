" .vimrc inspired by :
" Chewie : https://bitbucket.org/chewiebeardy/configs
" Delroth : https://bitbucket.org/delroth/configs

" General settings {{{
set encoding=utf-8

" Deactivate legacy vi compatibility
set nocompatible

" Set the prefix for many plugin commands
let mapleader="g"
let maplocalleader="ê"

" Toggle g option by default on substition
set gdefault

" Increase the maximum tab number while launching vim with -p
set tabpagemax=50

" Split behavior
set splitbelow
set splitright

" When scrolling, keep at list 5 lines visible on screen around the cursor
set scrolloff=5

" Make the backspace key have a sensible behavior
set backspace=indent,eol,start

" Check for vim settings embeded in the files we open
set modeline
set modelines=5

" Show selection size in visual mode
set showcmd

" Use fold markers by default
set fdm=marker

" Enhance command line completion
set wildmenu
" Set completion behavior, see :help wildmode for details
set wildmode=list:longest:full

" Disable bell completely
set visualbell
set t_vb=

" Highlight some special characters
set list
set listchars=tab:.\ ,nbsp:␣,precedes:«,extends:»
" Add a rule to highlight trailing whitespaces on every colorscheme we load
autocmd ColorScheme * highlight ExtraWhitespace ctermbg=darkgreen
autocmd ColorScheme * match ExtraWhitespace /\s\+$/

" Briefly show matching braces, parenthesis, etc
set showmatch

" Better vertical separator
set fillchars=vert:│

" Always show status line
set laststatus=2

" Show line number relative to the current one, but show absolute number for the
" current one
set number
set relativenumber

" Highlight the cursor line
set cursorline
" }}}

" bépo remaping {{{
if exists('+langnoremap') " see https://groups.google.com/forum/#!msg/vim_dev/QnNwLWhJ744/1qNcD7d9OvgJ
    source ~/.vimrc.bepo_langmap
else
    source ~/.vimrc.bepo_noremap
endif
" }}}

" Search options {{{

" Ignore case on search
set ignorecase

" Ignore case unless there is an uppercase letter in the pattern
set smartcase

" Move cursor to the matched string
set incsearch

" Don't highlight matched strings
set nohlsearch

" }}}

" Plugins (vundle) {{{
" Vundle setup
filetype off

set rtp+=~/.vim/bundle/vundle/
call vundle#begin()
Plugin 'gmarik/vundle'
Plugin 'tpope/vim-fugitive'
Plugin 'vim-syntastic/syntastic'
Plugin 'Valloric/YouCompleteMe'
Plugin 'vim-airline/vim-airline'
Plugin 'scrooloose/nerdcommenter'
Plugin 'SirVer/ultisnips'
Plugin 'airblade/vim-gitgutter'
Plugin 'ConradIrwin/vim-bracketed-paste'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'brookhong/cscope.vim'
Plugin 'pangloss/vim-javascript'
Plugin 'octol/vim-cpp-enhanced-highlight'
Plugin 'vim-python/python-syntax'
Plugin 'milkypostman/vim-togglelist'
Plugin 'lervag/vimtex'
call vundle#end()

" Enable filetype detection for plugins and indentation options
filetype plugin indent on

" ctrlp {{{
let g:ctrlp_by_filename = 1
let g:ctrlp_root_markers = ['package.json']
let g:ctrlp_custom_ignore = 'node_modules$'
" }}}

" vim-javascript {{{
let g:javascript_plugin_jsdoc = 1
augroup javascript_folding
    au!
    au FileType javascript setlocal foldmethod=syntax
augroup END
" }}}

" airline {{{
let g:airline#extensions#ycm#enabled = 1
let g:airline_powerline_fonts = 1
" }}}

"" Ultisnips {{{
let g:UltiSnipsExpandTrigger="<c-j>"
let g:UltiSnipsSnippetDirectories=["bundle/UltiSnips/UltiSnips", "custom_snippets"]
" }}}

"" YouCompleteMe {{{
let g:ycm_global_ycm_extra_conf="~/.vim/ycm_default_extra_conf.py"
let g:ycm_autoclose_preview_window_after_insertion=1
let g:ycm_always_populate_location_list = 1
" }}}

"" Syntastic {{{
let g:syntastic_c_check_header=1
let g:syntastic_cpp_check_header=1

let g:syntastic_ocaml_checkers = ['merlin']

" avoid conflicts with other plugins
let g:syntastic_java_checkers = []

" to much false errors with the latex checkers
let g:syntastic_tex_checkers = []
" }}}

"" Nerdcommenter {{{
let g:NERDSpaceDelims=1
" }}}

"" vimtex {{{
let g:vimtex_fold_enabled=1
let g:vimtex_view_automatic=0

" I redefine mappings in ftplugin/tex.vim
let g:vimtex_mappings_enabled=0
let g:vimtex_imaps_enabled=0

" Ignore some latex warnings
let g:vimtex_quickfix_latexlog = {'overfull' : 0, 'font' : 0, 'underfull' : 0}
" }}}

" Cscope {{{
nnoremap <leader>fa :call CscopeFindInteractive(expand('<cword>'))<CR>
" s: Find this C symbol
nnoremap  <leader>fs :call CscopeFind('s', expand('<cword>'))<CR>
" g: Find this definition
nnoremap  <leader>fg :call CscopeFind('g', expand('<cword>'))<CR>
" d: Find functions called by this function
nnoremap  <leader>fd :call CscopeFind('d', expand('<cword>'))<CR>
" c: Find functions calling this function
nnoremap  <leader>fc :call CscopeFind('c', expand('<cword>'))<CR>
" t: Find this text string
nnoremap  <leader>ft :call CscopeFind('t', expand('<cword>'))<CR>
" e: Find this egrep pattern
nnoremap  <leader>fe :call CscopeFind('e', expand('<cword>'))<CR>
" f: Find this file
nnoremap  <leader>ff :call CscopeFind('f', expand('<cword>'))<CR>
" i: Find files #including this file
nnoremap  <leader>fi :call CscopeFind('i', expand('<cword>'))<CR>
" }}}

"" merlin {{{
let g:opamshare = substitute(system('opam config var share'),'\n$','','''')
execute "set rtp+=" . g:opamshare . "/merlin/vim"
"}}}

" }}}

" Text formatting {{{
" Set text width for automatic wrapping and highlighting of maximum column
set textwidth=80
" But don't automatically wrap text and comments to textwidth.
set formatoptions-=tc
" Also don't display lines longer than the screen wraped, make me scroll
set nowrap
" Color the column *after* textwidth
set colorcolumn=+1
" }}}

" Persistent backups, undo files and cursor position {{{
set backup
set backupdir=~/.vimtmp/backup
set directory=~/.vimtmp/temp//

silent !mkdir -p ~/.vimtmp/backup
silent !mkdir -p ~/.vimtmp/temp

if version >= 703
    set undofile
    set undodir=~/.vimtmp/undo
    silent !mkdir -p ~/.vimtmp/undo
endif

" From the Vim wiki, restore cursor position
" http://vim.wikia.com/wiki/Restore_cursor_to_file_position_in_previous_editing_session
function! ResCur()
    if line("'\"") <= line("$")
        normal! g`"
        return 1
    endif
endfunction

augroup resCur
autocmd!
autocmd BufWinEnter * call ResCur()
augroup END
" }}}

" Syntax highlighting {{{
if has('syntax')
    syntax on
    set background=dark

    colorscheme delroth-molokai

    " Python specific options
    let python_highlight_builtins = 1
    let python_highlight_exceptions = 1
    let python_highlight_space_errors = 1

    " Ocaml specific options
    let g:syntastic_ocaml_use_ocamlbuild = 1

    " Stechec2 (Prologin) options
    let g:syntastic_cpp_include_dirs = ['/home/dettorer/work/prologin/stechec2/src/lib']
    let g:syntastic_cpp_compiler_options = ' -std=c++11'
endif
" }}}

" Indentation options {{{

" The display length of a tab character
set tabstop=8

" The number of spaces inserted when you press tab to indent with spaces
set softtabstop=4

" The number of spaces inserted/removed when using < or >
set shiftwidth=4

" Insert spaces instead of tabs
set expandtab

" When tabbing manually, use shiftwidth instead of tabstop and softtabstop
set smarttab

" Set basic indenting (i.e. copy the indentation of the previous line)
" When filetype detection didn't find a fancy indentation scheme
set autoindent

" C indentation rules. See :help cinoptions-values for details
set cinoptions=(0,u0,U0,t0,g0,N-s

" }}}

" Custom bindings {{{
" English spelling
noremap <F9> :!aspell -d english -e -c %<CR>
" French spelling
noremap <F10> :!aspell -d francais -e -c %<CR>

" Binding to paste mode (might not be needed with the bracketed-paste plugin)
noremap <F11> :set paste!<CR>
inoremap <F11> <C-O>:set paste!<CR>

" From Kalenz's Vim config: change pane with space key
nnoremap <Space> <C-w>
nnoremap <Space><Space> <C-w>w

" From halfr's: save with space key
nnoremap <Space><Return> :w<Return>
nnoremap <Space><Backspace> :x<Return>

" Bindings to save through sudo
cnoreabbrev w!! w !sudo tee % >/dev/null
nmap <Space>! :w!!<Return>

" Save and run make command on F5
nnoremap <F5> :w<Return>:make<Return>

" In addition to <leader>l and <leader>q from vim-togglelist, these bindings
" jump to next and previous item in either list
nnoremap <leader>nl :lnext<CR>
nnoremap <leader>pl :lprev<CR>
nnoremap <leader>nq :cnext<CR>
nnoremap <leader>pq :cprev<CR>
" }}}
