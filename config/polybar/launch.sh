#!/usr/bin/env sh

## Add this to your wm startup file.

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch Polybar
case "$(hostname)" in
    rivamar)
        polybar -c ~/.config/polybar/config.ini rivamar-top &
        polybar -c ~/.config/polybar/config.ini rivamar-bottom &
        # second screen
        polybar -c ~/.config/polybar/config.ini rivamar-top-alt &
        ;;
    frimapic)
        polybar -c ~/.config/polybar/config.ini frimapic-top-main &
        polybar -c ~/.config/polybar/config.ini frimapic-bottom-main &
        # second screen
        polybar -c ~/.config/polybar/config.ini frimapic-top-alt &
        polybar -c ~/.config/polybar/config.ini frimapic-bottom-alt &
        ;;
    *)
        polybar -c ~/.config/polybar/config.ini fallback &
        ;;
esac
