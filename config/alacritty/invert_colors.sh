#!/bin/sh
set -e

config="$HOME/.config/alacritty/alacritty.yml"
dark="colors_onehalfdark.yml"
light="colors_onehalflight.yml"

if [ -n "$(grep "$dark" $config)" ]; then
    echo "going light"
    sed -i "s/$dark/$light/" $config
else
    echo "going light"
    sed -i "s/$light/$dark/" $config
fi
