" .vimrc inspired by :
" Chewie : https://bitbucket.org/chewiebeardy/configs
" Delroth : https://bitbucket.org/delroth/configs

" General settings {{{
set encoding=utf-8

" Deactivate legacy vi compatibility
set nocompatible

" Set the prefix for many plugin commands
let mapleader=","
let maplocalleader="\\"

" Toggle g option by default on substition
set gdefault

" Increase the maximum tab number while launching vim with -p
set tabpagemax=50

" Split behavior
set splitbelow
set splitright

" When scrolling, keep at list 5 lines visible on screen around the cursor
set scrolloff=5

" Make the backspace key have a sensible behavior
set backspace=indent,eol,start

" Check for vim settings embeded in the files we open
set modeline
set modelines=5

" Show selection size in visual mode
set showcmd

" Use fold markers by default
set fdm=marker

" Enhance command line completion
set wildmenu
" Set completion behavior, see :help wildmode for details
set wildmode=list:longest:full

" Disable bell completely
set visualbell
set t_vb=

" Highlight some special characters
set list
set listchars=tab:.\ ,nbsp:␣,precedes:«,extends:»

" Briefly show matching braces, parenthesis, etc
set showmatch

" Better vertical separator
set fillchars=vert:│

" Always show status line
set laststatus=2

" Show line number relative to the current one, but show absolute number for the
" current one
set number
set relativenumber

" Highlight the cursor line
set cursorline

" Allow loading local .nvimrc files for custom configuration per project,
" but disallow :autocmd in them
set exrc
set secure

" Do not unload abandonned buffers, useful for Coc
set hidden

" Shorter time trigger for CursorHold (which makes coc-highlight show other uses
" of the symbol currently under the cursor)
set updatetime=300

" Disable concealing by default
set conceallevel=0

" Always show the sign column to avoid shifting the display when a diagnostic
" appears or disapears
set signcolumn=yes

" force the use of a specific python binary for extensions, so that when in a
" virtualenv or nix shell, neovim doesn't try to use the custom environment's
" python, which almost certainly doesn't provide the necessary modules.
let g:python_host_prog = '/usr/bin/python2'
let g:python3_host_prog = '/usr/bin/python3'
" }}}

" bépo remaping {{{
source ~/.config/nvim/bepo.vim
" }}}

" Search options {{{

" Ignore case on search
set ignorecase

" Ignore case unless there is an uppercase letter in the pattern
set smartcase

" Move cursor to the matched string
set incsearch

" Don't highlight matched strings
set nohlsearch

" }}}

" Plugins (vim-plug) {{{

" Old vundle setup
" filetype off

" set rtp+=~/.vim/bundle/vundle/
" call vundle#begin()
" Plugin 'ConradIrwin/vim-bracketed-paste'
" Plugin 'ctrlpvim/ctrlp.vim'
" Plugin 'brookhong/cscope.vim'
" Plugin 'milkypostman/vim-togglelist'
" call vundle#end()

set rtp+=~/.config/nvim/site " where vim-plug was manually installed
call plug#begin('~/.config/nvim/plugged')

" Engines
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}

" Themes and visuals
Plug 'rakr/vim-one'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'ryanoasis/vim-devicons'
Plug 'lukas-reineke/indent-blankline.nvim'

" Vim behaviors improvements
Plug 'tpope/vim-obsession'
Plug 'wellle/targets.vim'
Plug 'lambdalisue/suda.vim'
Plug 'tpope/vim-eunuch'
Plug 'milkypostman/vim-togglelist'
Plug 'kylechui/nvim-surround'

" Generic editing improvements
Plug 'junegunn/vim-easy-align'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'preservim/nerdtree'
Plug 'github/copilot.vim'

" Language-specific improvements
" TODO: a few plugins could be removed here if they only provide syntax
" highlighting (treesitter does its own highlighting for the languages it
" supports)
Plug 'justinmk/vim-syntax-extra'
Plug 'cespare/vim-toml'
Plug 'rust-lang/rust.vim'
Plug 'vim-python/python-syntax'
Plug 'tmhedberg/SimpylFold'
Plug 'Vimjas/vim-python-pep8-indent'
Plug 'psf/black', { 'branch': 'stable' }
Plug 'lervag/vimtex'
Plug 'pangloss/vim-javascript'
Plug 'octol/vim-cpp-enhanced-highlight'
Plug 'LnL7/vim-nix'
Plug 'glench/vim-jinja2-syntax'
Plug 'tridactyl/vim-tridactyl'
Plug 'fladson/vim-kitty'

" Dev tools
Plug 'scrooloose/nerdcommenter'
Plug 'janko-m/vim-test'
Plug 'SirVer/ultisnips'

call plug#end()

" Enable filetype detection for plugins and indentation options
filetype plugin indent on

" indent-blankline {{{
lua << EOF
require("ibl").setup {
    scope = {
        show_start = false
    }
}
EOF
" }}}

" nvim-surround {{{
lua << EOF
require("nvim-surround").setup({
    keymaps = {
        insert = "<C-g>s",
        insert_line = "<C-g>S",
        normal = "<leader>s",
        normal_cur = "<leader>ss",
        normal_line = "<leader>S",
        normal_cur_line = "<leader>SS",
        visual = "S",
        visual_line = "gS",
        delete = "ds",
        change = "cs",
        change_line = "cS",
    },
})
EOF
" }}}

" vim-javascript {{{
let g:javascript_plugin_jsdoc = 1
augroup javascript_folding
    au!
    au FileType javascript setlocal foldmethod=syntax
augroup END
" }}}

" airline {{{
let g:airline#extensions#ycm#enabled = 1
let g:airline_powerline_fonts = 1
" }}}

" Ultisnips {{{
let g:UltiSnipsExpandTrigger="<c-j>"
let g:UltiSnipsSnippetDirectories=["plugged/UltiSnips/UltiSnips", "custom_snippets"]
" }}}

" Nerdcommenter {{{
let g:NERDSpaceDelims=1
" }}}

" vimtex {{{
let g:vimtex_fold_enabled=1
let g:vimtex_view_automatic=0

" When in doubt between plaintex and latex, default to latex
let g:tex_flavor = 'latex'

" I redefine mappings in ftplugin/tex.vim
let g:vimtex_mappings_enabled=0
let g:vimtex_imaps_enabled=0

" Ignore some latex warnings
let g:vimtex_quickfix_ignore_filters = [
  \ 'Overfull',
  \ 'font',
  \ 'Underfull'
  \ ]
" }}}

" merlin {{{
" let g:opamshare = substitute(system('opam config var share'),'\n$','','''')
" execute "set rtp+=" . g:opamshare . "/merlin/vim"
" }}}

" vim-easy-align {{{
" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)

" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)
" }}}

" coc {{{
" Select autocompletion item with Tab and S-Tab, confirm with CR
inoremap <silent><expr> <TAB>
      \ coc#pum#visible() ? coc#pum#next(1):
      \ CheckBackspace() ? "\<Tab>" :
      \ coc#refresh()
inoremap <expr><S-TAB> coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"
inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"
function! CheckBackspace() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction
" Trigger a redraw every time the status line changes
autocmd User CocStatusChange redraws

" Code navigation
nmap <leader>ld <Plug>(coc-definition)
nmap <leader>lt <Plug>(coc-type-definition)
nmap <leader>li <Plug>(coc-implementation)
nmap <leader>lr <Plug>(coc-references-used)

" Code actions
vmap <leader>laf <Plug>(coc-format-selected)
nmap <leader>laf <Plug>(coc-format)
nmap <leader>lar <Plug>(coc-rename)
nmap <leader>laq <Plug>(coc-fix-current)

" Use K to show documentation in preview window.
nnoremap <silent> K :call ShowDocumentation()<CR>

function! ShowDocumentation()
  if CocAction('hasProvider', 'hover')
    call CocActionAsync('doHover')
  else
    call feedkeys('K', 'in')
  endif
endfunction

" Highlight other uses of the symbol currently under the cursor (needs
" coc-highlight)
autocmd CursorHold * silent call CocActionAsync('highlight')
highlight CocHighlightText guibg=#4b5263

" Workaround for coc bug that creates a misidentification of latex files in
" coc-ltex (the grammar and spell checking extension)
let g:coc_filetype_map = {'tex': 'latex'}
" }}}

" rust {{{
let g:rust_cargo_use_clippy = 1
" }}}

" python-syntax {{{
let g:python_highlight_all = 1
" }}}

" SimpylFold {{{
let g:SimpylFold_docstring_preview = 1
let g:SimpylFold_fold_docstring = 0
" }}}

" nvim-treesitter {{{
lua << EOF
require'nvim-treesitter.configs'.setup {
    ensure_installed = "all",
    sync_install = false,
    highlight = {
        enable = true,
        -- I prefer the default nvim highlighter for certain languages
        -- vimtex also relies on its own syntax highlighting being used for some
        -- of its features
        disable = { "markdown", "latex" },
    },
}
EOF
" }}}

" Copilot {{{
" Enable on a per-project basis
let g:copilot_enabled = v:false

" Remap the accept function from <Tab> to <C-Space>
imap <silent><script><expr> <C-Space> copilot#Accept("")
let g:copilot_no_tab_map = v:true
" }}}

" }}}

" Text formatting {{{
" Set text width for automatic wrapping and highlighting of maximum column
set textwidth=80
" But don't automatically wrap text and comments to textwidth.
set formatoptions-=tc
" Also don't display lines longer than the screen wraped, make me scroll
set nowrap
" Color the column *after* textwidth
set colorcolumn=+1
" }}}

" Persistent backups, undo files and cursor position {{{
set backup
set backupdir=~/.vimtmp/backup
set directory=~/.vimtmp/temp//

silent !mkdir -p ~/.vimtmp/backup
silent !mkdir -p ~/.vimtmp/temp

if version >= 703
    set undofile
    set undodir=~/.vimtmp/undo
    silent !mkdir -p ~/.vimtmp/undo
endif

" From the Vim wiki, restore cursor position
" http://vim.wikia.com/wiki/Restore_cursor_to_file_position_in_previous_editing_session
function! ResCur()
    if line("'\"") <= line("$")
        normal! g`"
        return 1
    endif
endfunction

augroup resCur
autocmd!
autocmd BufWinEnter * call ResCur()
augroup END
" }}}

" Syntax highlighting {{{
if has('syntax')
    " Add a rule to highlight trailing whitespaces on every colorscheme we load
    autocmd ColorScheme * highlight ExtraWhitespace ctermbg=darkgreen
    autocmd ColorScheme * match ExtraWhitespace /\s\+$/

    syntax enable

    " Truecolor handling
    set t_8f=[38;2;%lu;%lu;%lum
    set t_8b=[48;2;%lu;%lu;%lum
    set termguicolors

    " Colorscheme
    set background=dark
    let g:one_allow_italics=1
    colorscheme one
    let g:airline_theme='one'

    " Dim the default color for automatic inlay hints such as rust-analyzer's
    " type and chain hints
    hi CocHintSign ctermfg=12 guifg=#0b5963

    function! SwitchColorscheme()
        " Switch between a light and a dark colorscheme
        if &background ==# 'dark'
            set background=light
        else
            set background=dark
        endif
    endfunction
    noremap <F2> :call SwitchColorscheme()<CR>
endif
" }}}

" Indentation options {{{

" The display length of a tab character
set tabstop=8

" The number of spaces inserted when you press tab to indent with spaces
set softtabstop=4

" The number of spaces inserted/removed when using < or >
set shiftwidth=4

" Insert spaces instead of tabs
set expandtab

" When tabbing manually, use shiftwidth instead of tabstop and softtabstop
set smarttab

" Set basic indenting (i.e. copy the indentation of the previous line)
" When filetype detection didn't find a fancy indentation scheme
set autoindent

" C indentation rules. See :help cinoptions-values for details
set cinoptions=(0,u0,U0,t0,g0,N-s

" }}}

" Custom bindings {{{
" English spelling
noremap <F9> :setlocal spell spelllang=en<CR>
" French spelling
noremap <F10> :setlocal spell spelllang=fr<CR>

" Binding to paste mode (might not be needed with the bracketed-paste plugin)
noremap <F11> :set paste!<CR>
inoremap <F11> <C-O>:set paste!<CR>

" From Kalenz's Vim config: change pane with space key
nnoremap <Space> <C-w>
nnoremap <Space><Space> <C-w>w

" From halfr's: save with space key
if !exists('g:vscode')
    nnoremap <Space><Return> :w<Return>
    nnoremap <Space><Backspace> :x<Return>
else
    nnoremap <Space><Return> <cmd>call VSCodeCall('workbench.action.files.save')<CR>
    nnoremap <Space><Backspace> :x
endif

" Bindings to save through sudo
cnoreabbrev w!! w suda://%
nmap <Space>! :w!!<Return>

" Save and run make command on F5
nnoremap <F5> :w<Return>:make<Return>

" In addition to <leader>l and <leader>q from vim-togglelist, these bindings
" jump to next and previous item in either list
nnoremap <leader>nl :lnext<CR>
nnoremap <leader>pl :lprev<CR>
nnoremap <leader>nq :cnext<CR>
nnoremap <leader>pq :cprev<CR>

" One keystroke closer to command mode
noremap ; :
" More sensible yank bindings
nnoremap Y y$
" }}}
