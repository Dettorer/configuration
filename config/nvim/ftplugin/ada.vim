" The number of spaces inserted when you press tab
set softtabstop=3

" The number of spaces inserted/removed when using < or >
set shiftwidth=3
